<?php

//namespace mattbarber\Example;
include '../vendor/autoload.php';

use mattbarber\CountdownClock\Clock;
use mattbarber\CountdownClock\ClockInterface;

/**
 * Example class to inject into the countdown clock
 * Implements ClockInterface
 **/
class MyClock implements ClockInterface
{
    //Interbank
    //clock.php?deadline=1622383860&bgColor=255,255,255&dateColor=4,190,80&textColor=51,51,51&font=verdana&dateSize=60&textSize=14
    //Rimac
    //clock.php?deadline=1622383860&bgColor=255,255,255&dateColor=73,79,102&textColor=239,51,64&font=arial&dateSize=60&textSize=14

    public function getTypeFont()
    {
        return ( isset($_GET['font'])) ? strtolower($_GET['font']) : 'arial';;
    }

    /**
     * @return string clock name
     */
    public function getName()
    {
        $name = "My Super Clock";
        if(isset($_GET['name']) && $_GET['name']){$name = $_GET['name'];}
        return $name;
    }

    /**
     * @return /DateTime deadline date and time
     */
    public function getdeadlineDateTime()
    {
        $deadline = '28/04/2021 00:00:01';
        if(isset($_GET['deadline']) && $_GET['deadline']){$deadline = date('d/m/Y H:i:s', $_GET['deadline']);}

        return DateTime::createFromFormat('d/m/Y H:i:s', $deadline);
    }

    /**
     * @return string time zone
     */
    public function getTimezone()
    {
        $timezone = "America-Lima";
        if(isset($_GET['timezone']) && $_GET['timezone']){ $timezone = $_GET['timezone'];}
        return str_replace("-", "/", $timezone);
    }

    /**
     * @return string symbol between countdown date elements
     */
    public function getSeparator()
    {
        //$strSeparator = ":";
        $strSeparator = "0";
        $char = '';
        if(isset($_GET['strSeparator']) && $_GET['strSeparator']){ 
            $strSeparator = $_GET['strSeparator'];
        }

        switch(intval($strSeparator)){
            case 0:
                $char = ' ';
                break;
            case 1:
                $char = ':';
                break;
            case 2:
                $char = '|';
                break;
            case 3:
                $char = '/';
                break;
            case 4:
                $char = '-';
                break;
        }

        return $char;
    }

    /**
     * @return integer spacing between symbol between countdown date elements
     */
    public function getSeparatorSpacing()
    {
        $intSeparator = 0;
        if(isset($_GET['intSeparator']) && $_GET['intSeparator']){ $intSeparator = $_GET['intSeparator'];}
        return intval($intSeparator);
    }

    /**
     * @return integer length of days countdown date element
     */
    public function getDaysLen()
    {
        $daysLen = 2;
        if(isset($_GET['daysLen']) && $_GET['daysLen']){ $daysLen = $_GET['daysLen'];}
        return intval($daysLen);
    }

    /**
     * @return integer spacing between characters
     */
    public function getSpacing()
    {
        $charSpacing = 0;
        if(isset($_GET['charSpacing']) && $_GET['charSpacing']){ $charSpacing = $_GET['charSpacing'];}
        return intval($charSpacing);
    }

    /**
     * @return string font file path
     */
    public function getFontFilePath()
    {
        $fontType = ( isset($_GET['font'])) ? strtolower($_GET['font']) : 'arial';
        $font = dirname(__FILE__) . '/' . $fontType . '.ttf';
        //return "/verdana.ttf";
        return $font;
    }

    /**
     * @return integer size of font
     */
    public function getFontsize()
    {
        $fontSize = 60;
        if(isset($_GET['dateSize']) && $_GET['dateSize']){ $fontSize = $_GET['dateSize'];}
        return intval($fontSize);
    }

    /**
     * @return integer font start x
     */
    public function getFontx()
    {
        $pointX = 10;
        if(isset($_GET['pointX']) && $_GET['pointX']){ $pointX = $_GET['pointX'];}
        return intval($pointX);
    }

    /**
     * @return integer font start y
     */
    public function getFonty()
    {
        $pointY = 80;
        if(isset($_GET['pointY']) && $_GET['pointY']){ $pointY = $_GET['pointY'];}
        return intval($pointY);
    }

    /**
     * @return integer font red
     */
    public function getFontr()
    {
        $fontR = 51;
        if(isset($_GET['dateColor']) && $_GET['dateColor']){$fontR = explode(',', $_GET['dateColor']);}
        return intval($fontR[0]);
    }

    /**
     * @return integer font green
     */
    public function getFontg()
    {
        $fontR = 51;
        if(isset($_GET['dateColor']) && $_GET['dateColor']){$fontR = explode(',', $_GET['dateColor']);}
        return intval($fontR[1]);
    }

    /**
     * @return integer font blue
     */
    public function getFontb()
    {
        $fontR = 51;
        if(isset($_GET['dateColor']) && $_GET['dateColor']){$fontR = explode(',', $_GET['dateColor']);}
        return intval($fontR[2]);
    }

    /**
     * @return integer font angle
     */
    public function getFontangle()
    {
        return 0;
    }

    /**
     * @return string background image file path
     */
    public function getBackgroundImageFilePath()
    {
        if(isset($_GET['imagePath']) && $_GET['imagePath']){ 
            return $_GET['imagePath'];
        }
        return false;
    }

    /**
     * @return array [r, g, b] integer array if getBackgroundImageFilePath returns false
     *
     */
    public function getBackgroundImageColor()
    {
        $bgColor = [255, 255, 255];
        if(isset($_GET['bgColor']) && $_GET['bgColor']){$bgColor = explode(',', $_GET['bgColor']);}

        return $bgColor;
    }

    /**
     * @return array [r, g, b] integer array if getBackgroundImageFilePath returns false
     *
     */
    public function getTextColor()
    {
        $textColor = [0, 0, 0];
        if(isset($_GET['textColor']) && $_GET['textColor']){$textColor = explode(',', $_GET['textColor']);}

        return $textColor;
    }

    public function getTextsize()
    {
        $fontSize = 14;
        if(isset($_GET['textSize']) && $_GET['textSize']){ $fontSize = $_GET['textSize'];}
        return intval($fontSize);
    }
}


$clockItf = new MyClock();
$countdown = new Clock($clockItf);
//Call specifically (incase any further changes)
$countdown->generateImage();
